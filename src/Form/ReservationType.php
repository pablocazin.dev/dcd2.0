<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Validator\Constraints as Assert;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class)
            ->add('tel', TelType::class, [
                'attr' => [
                    'min' => '10',
                    'max' => '10',
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 10]),
                    new Assert\NotBlank()
                ],
                'invalid_message' => 'Le numéro doit contenir %num% chiffres',
                'invalid_message_parameters' => ['%num%' => '10'],
            ])
            ->add('jour', DateType::class, array(
                'widget' => 'choice',
                'years' => range(date('Y'), date('Y')+100),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
              ))
            ->add('nbCouverts', IntegerType::class, array(
                'attr' => array('min' => 1, 'max' => 100
                )))
            ->add('moment', ChoiceType::class, [
                'choices' => [
                    'midi' => 'midi',
                    'soir' => 'soir',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
