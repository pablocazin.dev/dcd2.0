<?php

namespace App\Controller;

use App\Entity\Encarts;
use App\Form\EncartsType;
use App\Repository\EncartsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/encarts')]
class EncartsController extends AbstractController
{
    #[Route('/', name: 'app_encarts_index', methods: ['GET'])]
    public function index(EncartsRepository $encartsRepository): Response
    {
        return $this->render('encarts/index.html.twig', [
            'encarts' => $encartsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_encarts_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EncartsRepository $encartsRepository): Response
    {
        $encart = new Encarts();
        $form = $this->createForm(EncartsType::class, $encart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encartsRepository->add($encart);
            return $this->redirectToRoute('app_encarts_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('encarts/new.html.twig', [
            'encart' => $encart,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_encarts_show', methods: ['GET'])]
    public function show(Encarts $encart): Response
    {
        return $this->render('encarts/show.html.twig', [
            'encart' => $encart,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_encarts_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Encarts $encart, EncartsRepository $encartsRepository): Response
    {
        $form = $this->createForm(EncartsType::class, $encart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encartsRepository->add($encart);
            return $this->redirectToRoute('app_encarts_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('encarts/edit.html.twig', [
            'encart' => $encart,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_encarts_delete', methods: ['POST'])]
    public function delete(Request $request, Encarts $encart, EncartsRepository $encartsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$encart->getId(), $request->request->get('_token'))) {
            $encartsRepository->remove($encart);
        }

        return $this->redirectToRoute('app_encarts_index', [], Response::HTTP_SEE_OTHER);
    }
}
