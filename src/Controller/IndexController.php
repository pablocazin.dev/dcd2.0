<?php

namespace App\Controller;

use App\Entity\Encarts;
use App\Form\Encarts1Type;
use App\Repository\EncartsRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(EncartsRepository $encartsRepository): Response
    {
        return $this->render('index/index.html.twig', [
            'encarts' => $encartsRepository->findAll(),
        ]);
    }
}
